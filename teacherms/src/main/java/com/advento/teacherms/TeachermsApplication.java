package com.advento.teacherms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TeachermsApplication {

	public static void main(String[] args) {
		SpringApplication.run(TeachermsApplication.class, args);
	}

}
