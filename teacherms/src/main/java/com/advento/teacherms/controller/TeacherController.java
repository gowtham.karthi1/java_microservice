package com.advento.teacherms.controller;

import java.util.Map;
import java.util.SortedSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.advento.teacherms.dto.TeacherDTO;
import com.advento.teacherms.service.TeacherService;

@RestController
public class TeacherController {

	@Autowired
	@Qualifier(value = "teacherServiceImpl")
	private TeacherService teacherService;

	@GetMapping(value  = "/teachersheight/{height}")
    public ResponseEntity<Map<Integer,SortedSet<TeacherDTO>>> getTeacherByHeight(@PathVariable(value="height") int heightVal) {
        Map<Integer,SortedSet<TeacherDTO>> teacherDTOs = teacherService.getAllTeacherByHeight(heightVal);
        ResponseEntity<Map<Integer,SortedSet<TeacherDTO>>> response = new ResponseEntity<Map<Integer,SortedSet<TeacherDTO>>>(teacherDTOs,HttpStatus.OK);
        return response;
    }
	
	@GetMapping(value = "/teachersage/{age}")
	public ResponseEntity<Map<Integer, SortedSet<TeacherDTO>>> getAllTeacherByAge(
			@PathVariable(value = "age") int ageVal) {
		Map<Integer, SortedSet<TeacherDTO>> teacherMapAge = teacherService.getAllTeacherByAge(ageVal);
		ResponseEntity<Map<Integer, SortedSet<TeacherDTO>>> responseEntity = new ResponseEntity<Map<Integer, SortedSet<TeacherDTO>>>(
				teacherMapAge, HttpStatus.OK);
		return responseEntity;

	}
	
	@GetMapping(value = "/teacherssalary/{salary}")
	public ResponseEntity<Map<Long, SortedSet<TeacherDTO>>> getAllTeacherBySalary(
			@PathVariable(value = "salary") int salaryVal) {
		Map<Long, SortedSet<TeacherDTO>> teacherMapSalary = teacherService.getAllTeacherBySalary(salaryVal);
		ResponseEntity<Map<Long, SortedSet<TeacherDTO>>> responseEntity = new ResponseEntity<Map<Long, SortedSet<TeacherDTO>>>(
				teacherMapSalary, HttpStatus.OK);
		return responseEntity;

	}
	
	@PostMapping(value = "/teacher")
	public ResponseEntity<Void> createTeacher(@RequestBody TeacherDTO teacherDTO) {
		teacherService.createTeacher(teacherDTO);
		ResponseEntity<Void> responseEntity = new ResponseEntity<Void>(HttpStatus.CREATED);
		return responseEntity;
	}
}
