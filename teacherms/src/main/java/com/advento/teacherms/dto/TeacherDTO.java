package com.advento.teacherms.dto;

public class TeacherDTO implements Comparable<TeacherDTO>{

	private int id;
	
	private String name;
	
	private int age;
	
	private String email;
	
	private int height;
	
	private long salary;

	@Override
	public String toString() {
		return "TeacherDTO [id=" + id + ", name=" + name + ", age=" + age + ", email=" + email + ", height=" + height
				+ ", salary=" + salary + "]";
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public long getSalary() {
		return salary;
	}

	public void setSalary(long salary) {
		this.salary = salary;
	}
	
//	@Override
//	public int hashCode() {
//		return email.hashCode();
//	}
//	
//	@Override
//	public boolean equals(Object obj) {
//		System.out.println("email----->"+((TeacherDTO) obj).getEmail());
//		return this.email.equals(((TeacherDTO) obj).getEmail());
//	}

	@Override
	public int compareTo(TeacherDTO teacherDTO) {
		int result= this.getEmail().compareTo(teacherDTO.getEmail());
//		System.out.println("Result--------->" + result);
		return result;
	}
	
}
