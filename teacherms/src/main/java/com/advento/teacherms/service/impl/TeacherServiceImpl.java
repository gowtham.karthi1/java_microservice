package com.advento.teacherms.service.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

import org.springframework.stereotype.Service;

import com.advento.teacherms.dto.TeacherDTO;
import com.advento.teacherms.service.TeacherService;

@Service(value = "teacherServiceImpl")
public class TeacherServiceImpl implements TeacherService {

	@Override
	public Map<Integer, SortedSet<TeacherDTO>> getAllTeacherByHeight(int heightVal) {
        Map<Integer, SortedSet<TeacherDTO>> mapofTeacher = new HashMap<>();
		try {
			Connection conn = getConnectionObj();
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery("select * from teacher_details where height="+ heightVal);
			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");
				int age = rs.getInt("age");
				String email = rs.getString("email");
				int height = rs.getInt("height");
				long salary = rs.getLong("salary");
				TeacherDTO teacherDTO = createTeacherObj(id, name, age, email, height, salary);
				addTeacherMapByHight(mapofTeacher,teacherDTO);
			}
		} catch (SQLException sqlEx) {
			sqlEx.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		return mapofTeacher;
}

	private Connection getConnectionObj() throws ClassNotFoundException, SQLException {
		
		Class.forName("com.mysql.jdbc.Driver");

		Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/teacherms", "root", "root");
		return conn;
	}
	private TeacherDTO createTeacherObj(int id, String name, int age, String email, int height, long salary) {
		TeacherDTO teacherDTO = new TeacherDTO();
		teacherDTO.setId(id);
		teacherDTO.setName(name);
		teacherDTO.setAge(age);
		teacherDTO.setEmail(email);
		teacherDTO.setHeight(height);
		teacherDTO.setSalary(salary);
		return teacherDTO;
	}
	
	public void addTeacherMapByHight(Map<Integer,SortedSet<TeacherDTO>> mapofTeacher,TeacherDTO teachermsDTO) {
    int height = teachermsDTO.getHeight();
    if (mapofTeacher.containsKey(height)) {
        SortedSet<TeacherDTO> newValList = mapofTeacher.get(height);
        newValList.add(teachermsDTO);
    }
    else {
        SortedSet<TeacherDTO> studentList = new TreeSet<>();
        studentList.add(teachermsDTO);
        mapofTeacher.put(height, studentList);
    }
}
	
	@Override
	public Map<Integer, SortedSet<TeacherDTO>> getAllTeacherByAge(int ageVal) {
        Map<Integer, SortedSet<TeacherDTO>> mapofTeacher = new HashMap<>();
		try {
			Connection conn = getConnectionObj();
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery("select * from teacher_details where age="+ ageVal);
			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");
				int age = rs.getInt("age");
				String email = rs.getString("email");
				int height = rs.getInt("height");
				long salary = rs.getLong("salary");
				TeacherDTO teacherDTO = createTeacherObj(id, name, age, email, height, salary);
				addTeacherMapByAge(mapofTeacher,teacherDTO);
			}
		} catch (SQLException sqlEx) {
			sqlEx.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		return mapofTeacher;
}

	public void addTeacherMapByAge(Map<Integer,SortedSet<TeacherDTO>> mapofTeacher,TeacherDTO teachermsDTO) {
    int age = teachermsDTO.getAge();
    if (mapofTeacher.containsKey(age)) {
        SortedSet<TeacherDTO> newValList = mapofTeacher.get(age);
        newValList.add(teachermsDTO);
    }
    else {
        SortedSet<TeacherDTO> studentList = new TreeSet<>();
        studentList.add(teachermsDTO);
        mapofTeacher.put(age, studentList);
    }
}
	
	@Override
	public Map<Long, SortedSet<TeacherDTO>> getAllTeacherBySalary(long salaryVal) {
        Map<Long, SortedSet<TeacherDTO>> mapofTeacher = new HashMap<>();
		try {
			Connection conn = getConnectionObj();
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery("select * from teacher_details where salary="+ salaryVal);
			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");
				int age = rs.getInt("age");
				String email = rs.getString("email");
				int height = rs.getInt("height");
				long salary = rs.getLong("salary");
				TeacherDTO teacherDTO = createTeacherObj(id, name, age, email, height, salary);
				addTeacherMapBySalary(mapofTeacher,teacherDTO);
			}
		} catch (SQLException sqlEx) {
			sqlEx.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		return mapofTeacher;
}

	public void addTeacherMapBySalary(Map<Long,SortedSet<TeacherDTO>> mapofTeacher,TeacherDTO teachermsDTO) {
    long salary = teachermsDTO.getSalary();
    if (mapofTeacher.containsKey(salary)) {
        SortedSet<TeacherDTO> newValList = mapofTeacher.get(salary);
        newValList.add(teachermsDTO);
    }
    else {
        SortedSet<TeacherDTO> studentList = new TreeSet<>();
        studentList.add(teachermsDTO);
        mapofTeacher.put(salary, studentList);
    }
}

	@Override
	public void createTeacher(TeacherDTO teacherDTO) {
		Connection conn = null;
		try {
			conn = getConnectionObj();

			PreparedStatement pStmt = conn.prepareStatement("insert into teacher_details (name, age, email, height, salary) values (?, ?, ?, ?, ?);");
			pStmt.setString(1, teacherDTO.getName());
			pStmt.setInt(2, teacherDTO.getAge());
			pStmt.setString(3, teacherDTO.getEmail());
			pStmt.setInt(4, teacherDTO.getHeight());
			pStmt.setLong(5, teacherDTO.getSalary());
			pStmt.execute();
		} catch (SQLException sqlEx) {
			sqlEx.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} finally {
			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}
}
