package com.advento.teacherms.service;

import java.util.Map;
import java.util.SortedSet;

import com.advento.teacherms.dto.TeacherDTO;

public interface TeacherService {

	public Map<Integer, SortedSet<TeacherDTO>> getAllTeacherByHeight(int heightVal);
	
	public Map<Integer, SortedSet<TeacherDTO>> getAllTeacherByAge(int ageVal);
	
	public Map<Long, SortedSet<TeacherDTO>> getAllTeacherBySalary(long salaryVal);
	
	public void createTeacher(TeacherDTO teacherDTO);
}
